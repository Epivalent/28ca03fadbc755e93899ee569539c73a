Blind Schnorr signature signing protocols are not concurrently secure by default due to the potential for certain types of attacks that exploit the concurrent execution of multiple protocol instances. Here are some key reasons why this is the case:

1. **Interleaving Attacks**: In a concurrent setting, an adversary can interleave messages from different protocol instances in a way that was not anticipated by the protocol designers. This can lead to situations where information from one instance leaks into another, potentially compromising security.

2. **Replay Attacks**: An attacker might reuse or replay messages from one session in another session. For example, if parts of the blind signature process are reused across different sessions without proper safeguards, it could allow an attacker to gain additional information or forge signatures.

3. **Malleability and Manipulation**: Blind Schnorr signatures involve random values and commitments that need to be carefully managed. In a concurrent environment, an adversary might manipulate these values across different sessions to create valid signatures on unintended messages.

4. **Lack of Session Separation**: If the protocol does not adequately separate different sessions (e.g., using unique session identifiers), then it becomes easier for an attacker to mix up components from different sessions, leading to potential vulnerabilities.

5. **Complexity in Proving Security**: The security proofs for cryptographic protocols often assume a sequential execution model where each instance runs independently and completes before another starts. Concurrent execution introduces additional complexity that makes it harder to prove security properties formally.

To achieve concurrent security for blind Schnorr signature protocols, additional measures must be taken:

- **Session Identifiers**: Use unique identifiers for each session to ensure that messages cannot be mixed between sessions.
- **Nonces and Randomness**: Ensure that all random values (nonces) used in the protocol are unique per session and cannot be reused or predicted.
- **Commitment Schemes**: Employ commitment schemes that bind specific values securely so they cannot be altered or misused across sessions.
- **Formal Analysis and Proofs**: Conduct formal analysis under models like Universal Composability (UC) which provide stronger guarantees about security even when multiple instances run concurrently.

By addressing these issues through careful design and rigorous analysis, it is possible to construct blind Schnorr signature protocols that remain secure even under concurrent execution scenarios.

# Sun  9 Jun 12:07:47 CEST 2024 - why are blind Schnorr signature signing protocols not concurrently secure by default? 